#include <iostream>
#include <cstdio>
#include <exception>

using namespace std;

class Excecao : public exception {
    const char * mensagem;
    Excecao(){}
public:
    // Forma compacta de declarar um contrutor já passando incluindo as atribuições após o ":"
	Excecao(const char * s) throw() : mensagem(s){};
	// Forma estendida de declaração do mesmo construtor
	/*
	 Excecao(const char * s) throw() {
		this->mensagem = s;
	 } 
	*/
    const char * what() const throw() {
        return mensagem;
    }
};

const Excecao e_ValorForaDosLimites("Valor fora dos limites");
const Excecao e_Overflow("Overflow de dados");

void funcao_com_erro(int tipo) {
	cout << endl << "Entrando na função com erros" << endl;
    switch (tipo) {
        case 1:
            throw (exception());
            break;
        case 2:
            throw Excecao("Erro: Excecao Personalizada");
            break;
        case 3:
            throw e_ValorForaDosLimites;
            break;
        default:
            break;
    }
}

int main( int argc, char ** argv ) {
    
	cout << "-------------------------" << endl << "Teste das exceções" << endl << "-------------------------" << endl;
    
    try {
        funcao_com_erro(1);
    }
	catch(Excecao &e) {
		cout << "Exceção: " << e.what() << endl;
	}
	catch(exception &e) {
		cout << "Exception: " << e.what() << endl;
	}
    
    try {
        funcao_com_erro(2);
    }
    catch (Excecao & e) {
        cout << "Excecao " << e.what() << endl;
    }

    try {
        funcao_com_erro(3);
    }
    catch (Excecao & e) {
        cout << "Excecao " << e.what() << endl;
    }
    
	cout << endl << "-------------------------" << endl << "Terminando a execução do programa" << endl << "-------------------------" << endl;

    return 0;
}
